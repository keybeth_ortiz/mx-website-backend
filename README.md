# Moneyxchange.io

> Backend para el website de Moneyxchange

---

Se requiere tener el plugin de [Maven](https://maven.apache.org/)

Para instalar las dependencias de Maven

    mvn install

### Para producción

Deben revisarse el archivo de properties **/src/main/resources/application-prod.properties**, que sean las indicadas para nuestro entorno, luego construir el empaquetado.

    mvn build

luego desplegar el war de la ruta **/target/website-*.war** en el servidor donde correrá nuestra aplicación, el cual debe correrse sustituyendo el profile con el argumento de la linea de comando

    --Dspring.profiles.active=prod.

### Para desarrollo

En el archivo **/src/main/resources/ddbb/mx_website.sql**, podemos conseguir un script con información para inicializar la base de datos, para el desarrollo y las pruebas.

Podemos correr directamente la clase `Application.java` y se activará el servidor embebido, la configuracion para este profile se encuentra en el archivo de properties **/src/main/resources/application-dev.properties**.