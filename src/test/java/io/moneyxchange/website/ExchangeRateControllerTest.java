package io.moneyxchange.website;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.nio.charset.Charset;

import javax.transaction.Transactional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import io.moneyxchange.website.config.Errors;
import io.moneyxchange.website.domain.ApplicationUser;
import io.moneyxchange.website.domain.Currency;
import io.moneyxchange.website.domain.ExchangeRate;
import io.moneyxchange.website.repositories.CurrencyRepository;
import io.moneyxchange.website.repositories.ExchangeRateRepository;
import io.moneyxchange.website.repositories.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
@TestPropertySource(locations = "classpath:application-test.properties")
@Transactional
public class ExchangeRateControllerTest {


    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mockMvc;
    
    @Autowired
    private ExchangeRateRepository exchangeRateRepository;
    
    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    
    @Autowired
    private UserRepository userRepository;
    
    private final Double EUR_RATE = 0.8148;
    private final Double AUD_RATE = 1.2809;

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        Currency dollar = new Currency();
        dollar.setSymbol("USD");
        dollar.setDescription("Dollar");
        currencyRepository.save(dollar);
        
        Currency euro = new Currency();
        euro.setSymbol("EUR");
        euro.setDescription("Euro");
        currencyRepository.save(euro);
        
        Currency australianDollar = new Currency();
        australianDollar.setSymbol("AUD");
        australianDollar.setDescription("Australian Dollar");
        currencyRepository.save(australianDollar);
        
        ExchangeRate er = new ExchangeRate();
        er.setBaseCurrency(dollar);
        er.setToCurrency(euro);
        er.setRate(EUR_RATE);        
        exchangeRateRepository.save(er);
        
        er = new ExchangeRate();
        er.setBaseCurrency(dollar);
        er.setToCurrency(australianDollar);
        er.setRate(AUD_RATE);        
        exchangeRateRepository.save(er);
        
        ApplicationUser user = new ApplicationUser();
        user.setEmail("keybeth@gmail.com");
        user.setPassword(bCryptPasswordEncoder.encode("123456789"));
        userRepository.save(user);
    }

    @Test
    public void paramsNotCompleted() throws Exception {
        mockMvc.perform(get("/api/rate/lastest"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.code", is(Errors.MISSING_PARAMETER.getCode())));
    }
    
    @Test
    public void currencyInvalid() throws Exception {
    	mockMvc.perform(get("/api/rate/lastest?base=OOO"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.code", is(Errors.INVALID_CURRENCY_SYMBOL.getCode())));
    }
    
    //@Test
    public void dataNotFound() throws Exception {
    	mockMvc.perform(get("/api/rate/lastest?base=EUR"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.code", is(Errors.INVALID_CURRENCY_SYMBOL.getCode())));
    }
    
    //@Test
    public void dataNotFound2() throws Exception {
    	mockMvc.perform(get("/api/rate/lastest?base=AUD&symbols=EUR"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.code", is(Errors.INVALID_CURRENCY_SYMBOL.getCode())));
    }

    @Test
    public void readHistoricalData() throws Exception {
    	mockMvc.perform(get("/api/rate/lastest?base=USD"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.base", is("USD")))
                .andExpect(jsonPath("$.rates.EUR", is(EUR_RATE)))
                .andExpect(jsonPath("$.rates.AUD", is(AUD_RATE)));
    }

    @Test
    public void readARate() throws Exception {
    	mockMvc.perform(get("/api/rate/lastest?base=USD&symbols=EUR"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.base", is("USD")))
                .andExpect(jsonPath("$.rates.EUR", is(EUR_RATE)));
    }
    
    @After
    public void tearDown() throws Exception {
    	exchangeRateRepository.deleteAll();
    	currencyRepository.deleteAll();
    	userRepository.deleteAll();
    }
}