package io.moneyxchange.website;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import io.moneyxchange.website.captcha.GoogleRecaptchaService;
import io.moneyxchange.website.config.Errors;
import io.moneyxchange.website.domain.ApplicationUser;
import io.moneyxchange.website.domain.Currency;
import io.moneyxchange.website.domain.ExchangeRate;
import io.moneyxchange.website.repositories.CurrencyRepository;
import io.moneyxchange.website.repositories.ExchangeRateRepository;
import io.moneyxchange.website.repositories.UserRepository;
import io.moneyxchange.website.security.JwtTokenHelper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
@TestPropertySource(locations = "classpath:application-test.properties")
@Transactional
public class SecurityTest {
	
	Logger logger = LoggerFactory.getLogger(SecurityTest.class);


    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mockMvc;
    
    @MockBean
	private GoogleRecaptchaService googleRecaptchaService;

    @SuppressWarnings("rawtypes")
	private HttpMessageConverter mappingJackson2HttpMessageConverter;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @Autowired
    private JwtTokenHelper tokenHelper;
    
    @Autowired
    CurrencyRepository currencyRepository;
    
    @Autowired
    ExchangeRateRepository exchangeRateRepository;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
            .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
            .findAny()
            .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }
    
    private ApplicationUser validUser,invalidUser;
    private final Double EUR_RATE = 0.8148;
    private final String VALID_PASSWORD = "123456789";
    private String tokenValid;

    @Before
    public void setup() throws Exception {    	
        this.mockMvc = webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
        validUser = new ApplicationUser();
        validUser.setEmail("keybeth@gmail.com");
        validUser.setPassword(bCryptPasswordEncoder.encode(VALID_PASSWORD));
        userRepository.save(validUser);        
        
        invalidUser = new ApplicationUser();
        invalidUser.setEmail("keybeth@hotmail.com");
        invalidUser.setPassword(bCryptPasswordEncoder.encode("12345678"));        

    	Map<String, Double> map = new HashMap<>();
    	map.put("EUR", EUR_RATE);

    	reset(googleRecaptchaService);
    	
    	Currency dollar = new Currency();
        dollar.setSymbol("USD");
        dollar.setDescription("Dollar");
        currencyRepository.save(dollar);
        
        Currency euro = new Currency();
        euro.setSymbol("EUR");
        euro.setDescription("Euro");
        currencyRepository.save(euro);
        
        ExchangeRate er = new ExchangeRate();
        er.setBaseCurrency(dollar);
        er.setToCurrency(euro);
        er.setRate(EUR_RATE);        
        exchangeRateRepository.save(er);
        
    	tokenValid = tokenHelper.build(validUser.getEmail(), new HashMap<String,Object>());
    	
    }

    private String getHeaderValue(String token){
    	return tokenHelper.getHeaderPrefix()+token;
    }
    private String getHeaderName(){
    	return tokenHelper.getHeaderName();
    }
    
    @Test
    public void notHeader() throws Exception {
    	mockMvc.perform(get("/api/rate/lastest?base=USD"))
                .andExpect(status().isUnauthorized())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.code", is(Errors.UNAUTHENTICATED.getCode())));
    }

    
    @Test
    public void headerWrong() throws Exception {
    	mockMvc.perform(get("/api/rate/lastest?base=USD").header(getHeaderName(), tokenValid))
		    	.andExpect(status().isUnauthorized())
		        .andExpect(content().contentType(contentType))
		        .andExpect(jsonPath("$.code", is(Errors.UNAUTHENTICATED.getCode())));
    }
    
    @Test
    public void tokenWrong() throws Exception {
    	String token = "koi786564657689iuygtyt8r5768978y";
    	mockMvc.perform(get("/api/rate/lastest?base=USD").header(getHeaderName(), getHeaderValue(token)))
                .andExpect(status().isUnauthorized())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.code", is(Errors.UNAUTHENTICATED.getCode())));
    }
    
    @Test
    public void tokenExpired() throws Exception {
    	String token = tokenHelper.build(validUser.getEmail(), new HashMap<String, Object>(), 1);
    	// Wait while expire the token
    	Thread.sleep(100);
    	mockMvc.perform(get("/api/rate/lastest?base=USD").header(getHeaderName(), getHeaderValue(token)))
		    	.andExpect(status().isUnauthorized())
		        .andExpect(content().contentType(contentType))
		        .andExpect(jsonPath("$.code", is(Errors.UNAUTHENTICATED.getCode())));
    }
    
    @Test
    public void authorizedRequest() throws Exception {
    	mockMvc.perform(get("/api/rate/lastest?base=USD").header(getHeaderName(), getHeaderValue(tokenValid)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.base", is("USD")))
                .andExpect(jsonPath("$.rates.EUR", is(EUR_RATE)));
    }

    @Test
    public void loginValid() throws Exception {
    	String validRecaptcha = "valid-re-captcha";
    	
    	//Use map because the ApplicationUser class doesn't expose the password field
    	Map<String, String> user = createUser(validUser.getEmail(), VALID_PASSWORD, validRecaptcha);    	
    	
    	given(this.googleRecaptchaService.validateResponse(validRecaptcha)).willReturn(true);
    	MvcResult result = mockMvc.perform(post("/login").content(json(user)))
    			.andExpect(status().isOk())
                .andReturn();
    	
    	String header = result.getResponse().getHeader(tokenHelper.getHeaderName());
    	
    	assertNotNull("The authorization header is null",header);
    	assertTrue("The header value doesn't starts with the token prefix",header.startsWith(tokenHelper.getHeaderPrefix()));
    	assertTrue("The user mismatch",tokenHelper.parseAndRetrieveClaims(header.replaceAll(tokenHelper.getHeaderPrefix(), "")).getSubject().equals(validUser.getEmail()));
    }
    
    @Test
    public void loginFailed() throws Exception {
    	String invalidRecaptcha = "invalid-re-captcha";

    	//Use map because the ApplicationUser class doesn't expose the password field
    	Map<String, String> user = createUser(null, null, null);
    	
    	mockMvc.perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(json(user)))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().contentType(contentType))
    	        .andExpect(jsonPath("$.code", is(Errors.VALIDATION.getCode())))
    	        .andExpect(jsonPath("$.errors[?(@.field == 'password')][?(@.type == 'NotEmpty')]").exists())
		        .andExpect(jsonPath("$.errors[?(@.field == 'password')][?(@.type == 'Minlength')]").exists())
    	        .andExpect(jsonPath("$.errors[?(@.field == 'email')][?(@.type == 'NotBlank')]").exists())
    	        .andExpect(jsonPath("$.errors[?(@.field == 'recaptcha')][?(@.type == 'NotBlank')]").exists());
    	
    	user = createUser("qwertyytterw", "1234", " ");
    	
    	mockMvc.perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(json(user)))
		        .andExpect(status().isUnprocessableEntity())
		        .andExpect(content().contentType(contentType))
		        .andExpect(jsonPath("$.code", is(Errors.VALIDATION.getCode())))
		        .andExpect(jsonPath("$.errors[?(@.field == 'password')][?(@.type == 'Minlength')]").exists())
		        .andExpect(jsonPath("$.errors[?(@.field == 'email')][?(@.type == 'Email')]").exists())
		        .andExpect(jsonPath("$.errors[?(@.field == 'recaptcha')][?(@.type == 'NotBlank')]").exists());
    	
    	user = createUser(validUser.getEmail(), "12345678901234567890", invalidRecaptcha);
    	
    	mockMvc.perform(post("/login").contentType(MediaType.APPLICATION_JSON).content(json(user)))
		        .andExpect(status().isUnprocessableEntity())
		        .andExpect(content().contentType(contentType))
		        .andExpect(jsonPath("$.code", is(Errors.VALIDATION.getCode())))
		        .andExpect(jsonPath("$.errors[?(@.field == 'password')][?(@.type == 'Maxlength')]").exists())
		        .andExpect(jsonPath("$.errors[?(@.field == 'email')][?(@.type == 'Email')]").doesNotExist())
		        .andExpect(jsonPath("$.errors[?(@.field == 'recaptcha')][?(@.type == 'NotBlank')]").doesNotExist())
		        .andExpect(jsonPath("$.errors[?(@.field == 'recaptcha')][?(@.type == 'ValidRecaptcha')]").exists());
    }
    
    
    
    @After
    public void tearDown() throws Exception {
    	userRepository.deleteAll();
    }
    
    protected Map<String, String> createUser(String email, String password, String recaptcha){
    	Map<String, String> user = new HashMap<>();
    	user.put("email", email);
    	user.put("password", password);
    	user.put("recaptcha", recaptcha);
    	return user;
    }

    @SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
}