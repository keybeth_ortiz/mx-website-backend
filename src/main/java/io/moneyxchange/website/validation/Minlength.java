package io.moneyxchange.website.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = MinlengthConstraintValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Minlength {
	
	String message() default "{io.moneyxchange.website.validation.Minlength.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    
    /**
	 * @return the min
	 */
    int value();
}
