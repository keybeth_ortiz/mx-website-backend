package io.moneyxchange.website.validation;

import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.hibernate.validator.internal.util.logging.Log;
import org.hibernate.validator.internal.util.logging.LoggerFactory;

public class MaxlengthConstraintValidator implements ConstraintValidator<Maxlength, String> {
	private  static final Log log = LoggerFactory.make();
	private int value;

	@Override
	public void initialize(Maxlength parameters) {
		this.value = parameters.value();
		validateParameters();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return Optional.ofNullable(value)
				.map(v -> v.length() <= this.value)
				.orElse(true);
	}
	
	private void validateParameters() {
		if ( this.value < 0 ) {
			throw log.getMaxCannotBeNegativeException();
		}
	}  
}
