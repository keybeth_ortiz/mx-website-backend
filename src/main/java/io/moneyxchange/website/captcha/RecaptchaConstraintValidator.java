package io.moneyxchange.website.captcha;

import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

public class RecaptchaConstraintValidator implements ConstraintValidator<ValidRecaptcha, String> {

	@Autowired
	private GoogleRecaptchaService googleRecaptchaService;

	@Override
	public void initialize(ValidRecaptcha parameters) {}

	@Override
	public boolean isValid(String response, ConstraintValidatorContext context) {
		return Optional.ofNullable(response)
				.filter(r-> ! r.trim().isEmpty())
				.map(r-> googleRecaptchaService.validateResponse(r))
				.orElse(false);
	}  
}
