package io.moneyxchange.website.captcha;

public interface GoogleRecaptchaService {
	
	public static final String VERIFY_URL = "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s";
	
	public boolean validateResponse(String response);
}
