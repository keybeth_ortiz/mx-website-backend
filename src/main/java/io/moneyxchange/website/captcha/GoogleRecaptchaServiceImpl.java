package io.moneyxchange.website.captcha;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

import io.moneyxchange.website.config.MyProperties;
import io.moneyxchange.website.domain.GoogleRecaptchaResponse;

@Service
public class GoogleRecaptchaServiceImpl implements GoogleRecaptchaService {
	@Autowired
    private HttpServletRequest request;
	
	@Autowired
	private MyProperties myProperties;
	
	@Autowired
    private RestOperations restTemplate;

	@Override
	public boolean validateResponse(String response) {
		String secretKey = myProperties.getRecaptcha().getSecret();
		
		URI verifyUri = URI.create(String.format(VERIFY_URL,secretKey, response, getClientIP()));
		
		GoogleRecaptchaResponse rs = restTemplate.getForObject(verifyUri, GoogleRecaptchaResponse.class);
		
		return rs.isSuccess();
	}
	
	private String getClientIP() {
        final String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null) {
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }

}
