package io.moneyxchange.website.resolvers;

import java.util.Optional;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ValueConstants;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver;

import io.moneyxchange.website.repositories.CurrencyRepository;

public class CurrencyArgumentResolver extends AbstractNamedValueMethodArgumentResolver {

	@Autowired
	private CurrencyRepository currencyRepository;	
	

	@Override
	protected NamedValueInfo createNamedValueInfo(MethodParameter parameter) {
		Currency annotation = parameter.getParameterAnnotation(Currency.class);
		return (annotation != null ? new NamedValueInfo(annotation.value(), annotation.required(), null) : new NamedValueInfo("",false,ValueConstants.DEFAULT_NONE));
	}

	@Override
	protected Object resolveName(String name, MethodParameter parameter, NativeWebRequest request) throws Exception {
		Optional<String> optionalValue = Optional.ofNullable(request.getParameter(name));
		if(optionalValue.isPresent()){
			return optionalValue
					 .map(value -> currencyRepository.findBySymbol(value))
					 .map(currency -> currency.getSymbol())
					 .orElseThrow(InvalidCurrencyException::new);
		}
		return null;
	}

	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		Currency annotation = parameter.getParameterAnnotation(Currency.class);
		return Optional.ofNullable(annotation)
				.map(Currency::value)
				.filter(name -> name.trim().length() > 0)
				.isPresent();
	}
	
	@Override
	protected void handleMissingValue(String name, MethodParameter parameter) throws ServletException {
		throw new MissingServletRequestParameterException(name, parameter.getParameterType().getSimpleName());
	}
}
