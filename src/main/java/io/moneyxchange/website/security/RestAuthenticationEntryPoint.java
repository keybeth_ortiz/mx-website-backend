package io.moneyxchange.website.security;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.moneyxchange.website.config.Errors;
import io.moneyxchange.website.domain.ExceptionResponse;

/**
 * This is invoked when user tries to access a secured REST resource without supplying any credentials
 * We should just send a 401 Unauthorized or validation failed response because there is no 'login page'
 * to redirect to
 */
@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

	private static final long serialVersionUID = 5907023648091540313L;
	
	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

	@Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authException) throws IOException {
        
		ExceptionResponse er;
		if(authException instanceof LoginConstraintViolationException  ){
			er = new ExceptionResponse(Errors.VALIDATION, ((LoginConstraintViolationException) authException).getBindingResult());  	
			response.setStatus(422); //UNPROCESSABLE_ENTITY
		}else{
			er = new ExceptionResponse(Errors.UNAUTHENTICATED);  	
	        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		}		
        response.setContentType(contentType.toString());
        new ObjectMapper().writeValue(response.getOutputStream(), er);
    }
}