package io.moneyxchange.website.security;

import java.io.IOException;
import java.nio.charset.Charset;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.moneyxchange.website.config.Errors;
import io.moneyxchange.website.domain.ExceptionResponse;

public class RestAuthenticationFailureHandler implements AuthenticationFailureHandler {
	
	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		ExceptionResponse er;
		if(exception instanceof LoginConstraintViolationException  ){
			er = new ExceptionResponse(Errors.VALIDATION, ((LoginConstraintViolationException) exception).getBindingResult());  	
			response.setStatus(422); //UNPROCESSABLE_ENTITY
		}else{
			er = new ExceptionResponse(Errors.UNAUTHENTICATED);  	
	        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		}	
        response.setContentType(contentType.toString());
        new ObjectMapper().writeValue(response.getOutputStream(), er);
		
	}

}
