package io.moneyxchange.website.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.validation.DataBinder;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.moneyxchange.website.domain.ApplicationUser;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	
    private final AuthenticationManager authenticationManager;
	
	private final JwtTokenHelper tokenHelper;
	
    private final LocalValidatorFactoryBean validator;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager,JwtTokenHelper tokenHelper,LocalValidatorFactoryBean validator) {
        this.authenticationManager = authenticationManager;
        this.tokenHelper = tokenHelper;
        this.validator = validator;
        this.setAuthenticationFailureHandler(new RestAuthenticationFailureHandler());
    }
    
    public ApplicationUser validateUser(ApplicationUser user) {
    	DataBinder binder = new DataBinder(user);
    	binder.setValidator(validator);
    	binder.validate(ApplicationUser.Login.class);
    	if (binder.getBindingResult().hasErrors()) {
    	    throw new LoginConstraintViolationException(binder.getBindingResult());
    	}
    	return user;
	}

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,HttpServletResponse response) throws AuthenticationException {
        try {
            ApplicationUser user = new ObjectMapper().readValue(request.getInputStream(), ApplicationUser.class);
            
            validateUser(user);

            return authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getEmail(),user.getPassword(),new ArrayList<>())
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,HttpServletResponse response,FilterChain chain,Authentication authResult) throws IOException, ServletException {
    	User user = (User) authResult.getPrincipal();
    	
    	Map<String, Object> additionalClaims = new HashMap<String, Object>();
    	additionalClaims.put("roles", user.getAuthorities().stream().map(a -> a.getAuthority()).collect(Collectors.toList()));    	

        String token = tokenHelper.build(user.getUsername(), additionalClaims);
        response.addHeader(tokenHelper.getHeaderName(), tokenHelper.getHeaderPrefix() + token);
    }
}
