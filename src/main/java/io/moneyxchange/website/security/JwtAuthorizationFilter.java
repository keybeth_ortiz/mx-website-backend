package io.moneyxchange.website.security;

import java.io.IOException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {
	
	private final JwtTokenHelper tokenHelper;
	
    public JwtAuthorizationFilter(AuthenticationManager authenticationManager,  JwtTokenHelper tokenHelper) {
		super(authenticationManager);
        this.tokenHelper = tokenHelper;
	}

    @Override
    protected void doFilterInternal(HttpServletRequest request,HttpServletResponse response,FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(tokenHelper.getHeaderName());

        if (header == null || !header.startsWith(tokenHelper.getHeaderPrefix())) {
            chain.doFilter(request, response);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(request);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
    	try{
	        String token = request.getHeader(tokenHelper.getHeaderName());
	        if (token != null) {
	            // parse the token.
	        	Claims claims = tokenHelper.parseAndRetrieveClaims(token);
        	
	        	String user = tokenHelper.getUserFromClaims(claims);
	        	
	        	// We also can use the last info or search the user in the DDBB, not in this case	
	        	List<GrantedAuthority> authorities = tokenHelper.getAuthoritiesFromClaims(claims);
	        	
	            if ( user != null) {
	                return new UsernamePasswordAuthenticationToken(user, null, authorities);
	            }
	            return null;
	        }
	        return null;

    	}catch (JwtException e) {
    		return null;
		}
    }
}
