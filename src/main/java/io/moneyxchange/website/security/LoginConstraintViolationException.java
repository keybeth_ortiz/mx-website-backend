package io.moneyxchange.website.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import io.moneyxchange.website.domain.ApplicationUser;

/**
 * Exception throw when fail the validation over {@link ApplicationUser} in login process
 * 
 */
public class LoginConstraintViolationException extends AuthenticationException {

	private static final long serialVersionUID = -386471504307588569L;

	private final BindingResult bindingResult;

	/**
	 * Constructor for {@link LoginConstraintViolationException}.
	 * @param bindingResult the results of the validation
	 */
	public LoginConstraintViolationException(BindingResult bindingResult/*Set<? extends ConstraintViolation<?>> constraintViolations*/) {
		super("Login Constraint Violation Exception");
		this.bindingResult = bindingResult;
	}
	
	/**
	 * Return the results of the failed validation.
	 */
	public BindingResult getBindingResult() {
		return this.bindingResult;
	}
	
	@Override
	public String getMessage() {
		StringBuilder sb = new StringBuilder("Validation failed for login ")
			.append(", with ").append(this.bindingResult.getErrorCount()).append(" error(s): ");
		for (ObjectError error : this.bindingResult.getAllErrors()) {
			sb.append("[").append(error).append("] ");
		}
		return sb.toString();
	}


}
