package io.moneyxchange.website.security;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.moneyxchange.website.config.MyProperties;

@Component
public class JwtTokenHelper {
	
	@Autowired
	private MyProperties myProperties;
	
	public String build(String sub, Map<String, Object> additionalClaims, long expirationTime) {
		return Jwts.builder()
        .setSubject(sub)
        .setExpiration(new Date(System.currentTimeMillis() + expirationTime))
        .signWith(SignatureAlgorithm.HS512, myProperties.getSecurity().getSecret().getBytes())
        .addClaims(additionalClaims)
        .compact();
	}
	
	public String build(String sub, Map<String, Object> additionalClaims) {
		return build(sub, additionalClaims, myProperties.getSecurity().getExpirationTime());
	}
	
	public Claims parseAndRetrieveClaims(String token) {
		return Jwts.parser()
                .setSigningKey(myProperties.getSecurity().getSecret().getBytes())
                .parseClaimsJws(token.replace(myProperties.getSecurity().getTokenPrefix(), ""))
                .getBody();
	}
	
	public String getUserFromClaims(Claims claims){
		return claims.getSubject();
	}
	
	public String getHeaderName(){
		return myProperties.getSecurity().getHeaderString();
	}
	
	public String getHeaderPrefix(){
		return myProperties.getSecurity().getTokenPrefix();
	}
	
	public String getSignatureSecret(){
		return myProperties.getSecurity().getSecret();
	}
	
	@SuppressWarnings("unchecked")
	public List<GrantedAuthority> getAuthoritiesFromClaims(Claims claims){
		if(claims.containsKey("roles"))
			return ((List<String>) claims.get("roles",List.class)).stream()	        			
					.map(role -> new SimpleGrantedAuthority(role)).collect(Collectors.toList());
		else
			return new ArrayList<GrantedAuthority>();
	}
}
