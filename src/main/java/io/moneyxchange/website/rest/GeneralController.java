package io.moneyxchange.website.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.moneyxchange.website.config.ApplicationPropertiesExposed;
import io.moneyxchange.website.config.MyProperties;

@RestController
@RequestMapping(value="/api")
public class GeneralController {
	
	@Autowired
	private MyProperties myProperties;

	@GetMapping("/properties")
	public ApplicationPropertiesExposed getProperties(){
		return new ApplicationPropertiesExposed(myProperties);	
	}
}
