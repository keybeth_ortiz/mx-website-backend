package io.moneyxchange.website.rest;

import java.util.Date;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.moneyxchange.website.domain.RateResponse;
import io.moneyxchange.website.resolvers.Currency;
import io.moneyxchange.website.service.ExchangeRateService;

@RestController
@RequestMapping(value="/api/rate")
public class ExchangeRateController {
	
	@Autowired
	private ExchangeRateService exchangeRateService;

	@GetMapping("/lastest")
	public RateResponse getRate(
		@Currency(value = "base") String baseCurrencySymbol,
		@Currency(value = "symbols", required = false) String toCurrencySymbol){
		Map<String, Double> rates;
		
		if(Optional.ofNullable(toCurrencySymbol).isPresent()){
			rates = exchangeRateService.quoteRate(baseCurrencySymbol, toCurrencySymbol);
		}else{
			rates = exchangeRateService.getActualRatesFromCurrency(baseCurrencySymbol);
		}
		 
		return new RateResponse(baseCurrencySymbol,new Date(), rates);		
	}
}
