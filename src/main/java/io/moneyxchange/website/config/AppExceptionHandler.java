package io.moneyxchange.website.config;

import java.util.Optional;

import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import io.moneyxchange.website.domain.ExceptionResponse;
import io.moneyxchange.website.resolvers.InvalidCurrencyException;
import io.moneyxchange.website.security.LoginConstraintViolationException;

@SuppressWarnings("deprecation")
@ControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.VALIDATION,ex.getBindingResult());
		return handleExceptionInternal(ex, er, headers, HttpStatus.UNPROCESSABLE_ENTITY, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.MISSING_PARAMETER.parameters(ex.getParameterName()));
		return handleExceptionInternal(ex, er, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleNoSuchRequestHandlingMethod(NoSuchRequestHandlingMethodException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.NO_SUCH_REQUEST_HANDLING_METHOD);
		return handleExceptionInternal(ex, er, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.REQUEST_METHOD_NOT_SUPPORTED);
		return handleExceptionInternal(ex, er, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.HTTP_MEDIA_TYPE_NOT_SUPPORTED);
		return handleExceptionInternal(ex, er, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleHttpMediaTypeNotAcceptable(HttpMediaTypeNotAcceptableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.HTTP_MEDIA_TYPE_NOT_ACCEPTABLE);
		return handleExceptionInternal(ex, er, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleMissingPathVariable(MissingPathVariableException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.MISSING_PATH_VARIABLE);
		return handleExceptionInternal(ex, er, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleServletRequestBindingException(ServletRequestBindingException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.SERVLET_REQUEST_BINDING);
		return handleExceptionInternal(ex, er, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleConversionNotSupported(ConversionNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.CONVERSION_NOT_SUPPORTED);
		return handleExceptionInternal(ex, er, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.TYPE_MISMATCH);
		return handleExceptionInternal(ex, er, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.HTTP_MESSAGE_NOT_READABLE);
		return handleExceptionInternal(ex, er, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotWritable(HttpMessageNotWritableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.HTTP_MESSAGE_NOT_WRITABLE);
		return handleExceptionInternal(ex, er, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleMissingServletRequestPart(MissingServletRequestPartException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.MISSING_SERVLET_REQUEST_PART);
		return handleExceptionInternal(ex, er, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status,
			WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.BINDING);
		return handleExceptionInternal(ex, er, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.NO_HANDLER_FOUND);
		return handleExceptionInternal(ex, er, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleAsyncRequestTimeoutException(AsyncRequestTimeoutException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.ASYNC_REQUEST_TIMEOUT);
		return handleExceptionInternal(ex, er, headers, status, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		if(! Optional.of(body).filter(b -> b  instanceof ExceptionResponse).isPresent() ){
			body = new ExceptionResponse(Errors.INTERNAL_ERROR_SERVER);
		}
		return super.handleExceptionInternal(ex, body, headers, status, request);
	}
	
	@ExceptionHandler({ LoginConstraintViolationException.class })
	public ResponseEntity<Object> handleLoginConstraintViolationException(LoginConstraintViolationException ex,
			HttpHeaders headers, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.VALIDATION,ex.getBindingResult());
		return handleExceptionInternal(ex, er, headers, HttpStatus.UNPROCESSABLE_ENTITY, request);
	}
	
	@ExceptionHandler({ AccessDeniedException.class })
	public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.UNAUTHORIZED);
		return handleExceptionInternal(ex, er, new HttpHeaders(), HttpStatus.FORBIDDEN, request);
	}

	@ExceptionHandler({ AuthenticationException.class})
	public ResponseEntity<Object> handleAuthenticationException(AuthenticationException ex,  WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.UNAUTHENTICATED);
		return handleExceptionInternal(ex, er, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
	}
	
	@ExceptionHandler({ InvalidCurrencyException.class})
	public ResponseEntity<Object> handleInvalidCurrencyException(InvalidCurrencyException ex,  WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.INVALID_CURRENCY_SYMBOL);
		return handleExceptionInternal(ex, er, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}
	
	

	@ExceptionHandler(value = { Exception.class, NullPointerException.class })
	protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
		ExceptionResponse er = new ExceptionResponse(Errors.INTERNAL_ERROR_SERVER);
		return handleExceptionInternal(ex, er, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR,request);
	}
}
