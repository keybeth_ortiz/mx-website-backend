package io.moneyxchange.website.config;

import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import io.moneyxchange.website.security.JwtAuthenticationFilter;
import io.moneyxchange.website.security.JwtAuthorizationFilter;
import io.moneyxchange.website.security.JwtTokenHelper;
import io.moneyxchange.website.security.RestAuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
    private RestAuthenticationEntryPoint unauthorizedHandler;
	
	@Autowired
	private JwtTokenHelper tokenHelper;
	
	@Autowired
	private Validator validator;
	
	@Bean
	public ValidatorFactory validatorFactory(){
		return new LocalValidatorFactoryBean();
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
  	CorsConfigurationSource corsConfigurationSource() {
	    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	    CorsConfiguration corsConfig = new CorsConfiguration().applyPermitDefaultValues();
	    corsConfig.addExposedHeader(tokenHelper.getHeaderName());
	    source.registerCorsConfiguration("/**", corsConfig);
	    return source;
	}	

	@Bean
	@Override
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}
	
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.cors()
			.and()
				.addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class)
				.addFilterBefore(authorizationFilter(), UsernamePasswordAuthenticationFilter.class)
				.exceptionHandling()
					.authenticationEntryPoint(unauthorizedHandler)
			.and()
				.sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
				.csrf().disable()
				.authorizeRequests()
					.antMatchers(HttpMethod.POST,"/login").permitAll()
					.antMatchers(HttpMethod.GET,"/api/properties").permitAll()
					.anyRequest().authenticated()
			.and()
				.headers().cacheControl();
	}
	
	private JwtAuthenticationFilter authenticationFilter() throws Exception{
		return new JwtAuthenticationFilter(authenticationManager(),tokenHelper, (LocalValidatorFactoryBean) validator);
	}
	
	private JwtAuthorizationFilter authorizationFilter() throws Exception{
		return new JwtAuthorizationFilter(authenticationManager(), tokenHelper);
	}
}
