package io.moneyxchange.website.config;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

public enum Errors {

	INTERNAL_ERROR_SERVER(1),
	VALIDATION(2),
	UNAUTHORIZED(3),
	UNAUTHENTICATED(4),
	MISSING_PARAMETER(5),
	REQUEST_METHOD_NOT_SUPPORTED(6),
	HTTP_MEDIA_TYPE_NOT_SUPPORTED(7),
	HTTP_MEDIA_TYPE_NOT_ACCEPTABLE(8),
	MISSING_PATH_VARIABLE(9),
	SERVLET_REQUEST_BINDING(10),
	CONVERSION_NOT_SUPPORTED(11),
	TYPE_MISMATCH(12),
	HTTP_MESSAGE_NOT_WRITABLE(13),
	MISSING_SERVLET_REQUEST_PART(14),
	BINDING(15),
	NO_HANDLER_FOUND(16),
	ASYNC_REQUEST_TIMEOUT(17),
	NO_SUCH_REQUEST_HANDLING_METHOD(18),
	HTTP_MESSAGE_NOT_READABLE(19),
	INVALID_CURRENCY_SYMBOL(20)
	;
	
	private MessageSource messageSource;
	
	private int code;
	private Object[] arguments = {};
	
	private Errors(int code) {
		this.code = code;
	}

	public Errors parameters(Object... arguments){		  
		this.arguments = arguments;
		return this;
	}
	
	@Component
    public static class ErrorsInjector {
        
		@Autowired
        private MessageSource messageSource;

        @PostConstruct
        public void postConstruct() {
            for (Errors rt : EnumSet.allOf(Errors.class)){
               rt.setMessageSource(messageSource);
            }
        }
    }

	public int getCode() {
		return code;
	}

	public String getMessage() {
		Locale locale = LocaleContextHolder.getLocale();
		String code = "error."+this.name().toLowerCase().replace("_", "-");
		return messageSource.getMessage(code, arguments, locale);
	}
	
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}	
	
}
