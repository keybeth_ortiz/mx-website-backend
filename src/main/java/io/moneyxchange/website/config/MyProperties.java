package io.moneyxchange.website.config;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Component
@ConfigurationProperties(prefix="my")
@Validated
public class MyProperties {

	@Valid
	private Security security = new Security();
	
	@Valid
	private Recaptcha recaptcha = new Recaptcha();
	
    public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public Recaptcha getRecaptcha() {
		return recaptcha;
	}

	public void setRecaptcha(Recaptcha recaptcha) {
		this.recaptcha = recaptcha;
	}



	public static class Security{
		
		public static final long DEFAULT_EXPIRATION_TIME = 1 * 3600 * 1000; //A hour
		public static final String DEFAULT_TOKEN_PREFIX = "Bearer ";
		public static final String DEFAULT_HEADER_STRING = "Authorization";
    	
		/**
		 * Secret key for sign the JWT token
		 */
    	@NotEmpty	
    	private String secret;
    	
    	/**
		 * JWT token expire time in ms
		 */
    	private long expirationTime = DEFAULT_EXPIRATION_TIME;
    	
    	/**
		 * JWT token prefix in the request header
		 */
    	private String tokenPrefix = DEFAULT_TOKEN_PREFIX;
    	
    	/**
		 * JWT token header's name
		 */
    	private String headerString = DEFAULT_HEADER_STRING;
        
        
	    public String getSecret() {
			return secret;
		}

		public void setSecret(String secret) {
			this.secret = secret;
		}

		public long getExpirationTime() {
			return expirationTime;
		}

		public void setExpirationTime(long expirationTime) {
			this.expirationTime = expirationTime;
		}

		public String getTokenPrefix() {
			return tokenPrefix;
		}

		public void setTokenPrefix(String tokenPrefix) {
			this.tokenPrefix = tokenPrefix;
		}

		public String getHeaderString() {
			return headerString;
		}

		public void setHeaderString(String headerString) {
			this.headerString = headerString;
		}
	}
	
	public static class Recaptcha{
		
		/**
		 * Google recaptcha site key 
		 */
		@NotEmpty
		private String key;
		
		/**
		 * Google recaptcha secret key 
		 */
		@NotEmpty
		private String secret;

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getSecret() {
			return secret;
		}

		public void setSecret(String secret) {
			this.secret = secret;
		}
	}
    
}
