package io.moneyxchange.website.config;

import java.util.List;

import javax.validation.ValidatorFactory;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.client.RestOperations;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import io.moneyxchange.website.resolvers.CurrencyArgumentResolver;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {	

	
	@Bean
	public ValidatorFactory validatorFactory(){
		return new LocalValidatorFactoryBean();
	}
	
	@Bean
	public CurrencyArgumentResolver currencyArgumentResolver() {
	    return new CurrencyArgumentResolver();
	}
	
	@Bean
	public RestOperations restTemplate(RestTemplateBuilder builder) {
	   return builder.build();
	}
 
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(currencyArgumentResolver());
    }
}
