package io.moneyxchange.website.config;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApplicationPropertiesExposed {
	
	@JsonProperty("RECAPTCHA_KEY")
	private final String recaptchaKey;
	
	@JsonProperty("AUTHORIZATION_TOKEN_PREFIX")
	private final String authorizationTokenPrefix;
	
	@JsonProperty("AUTHORIZATION_HEADER_NAME")
	private final String authorizationHeaderName;
	
	public ApplicationPropertiesExposed( MyProperties myProperties) {
		this.recaptchaKey = myProperties.getRecaptcha().getKey();
		this.authorizationTokenPrefix = myProperties.getSecurity().getTokenPrefix();
		this.authorizationHeaderName = myProperties.getSecurity().getHeaderString();
	}

	public String getRecaptchaKey() {
		return recaptchaKey;
	}

	public String getAuthorizationTokenPrefix() {
		return authorizationTokenPrefix;
	}

	public String getAuthorizationHeaderName() {
		return authorizationHeaderName;
	}
}
