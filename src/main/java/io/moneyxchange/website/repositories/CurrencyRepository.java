package io.moneyxchange.website.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import io.moneyxchange.website.domain.Currency;

public interface CurrencyRepository extends PagingAndSortingRepository<Currency, Long>{
	
	public Currency findBySymbol(String symbol);
}
