package io.moneyxchange.website.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import io.moneyxchange.website.domain.ApplicationUser;

public interface UserRepository extends PagingAndSortingRepository<ApplicationUser, Long>{
	public ApplicationUser findByEmail(String email);
}
