package io.moneyxchange.website.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import io.moneyxchange.website.domain.ExchangeRate;

public interface ExchangeRateRepository extends PagingAndSortingRepository<ExchangeRate, Long>{

	/**
	 * Find the last exchange rate between two currencies
	 * 
	 * @param baseCurrency
	 * @param toCurrency
	 * @return
	 */
	public ExchangeRate findByBaseCurrencySymbolAndToCurrencySymbol(String baseCurrency, String toCurrency);
	
	/**
	 * Find last exchange rate of the currencies for the given currency  
	 * 
	 * @param baseCurrency
	 * @return
	 */
	public List<ExchangeRate> findByBaseCurrencySymbol(String baseCurrency);

}
