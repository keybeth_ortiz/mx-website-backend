package io.moneyxchange.website.domain;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.moneyxchange.website.config.Errors;

@JsonInclude(Include.NON_NULL)
public class ExceptionResponse {
	private int code;
	private String message;
	private List<FieldError> errors;
	
	
	public ExceptionResponse() {
		super();
	}
	
	public ExceptionResponse(int code, String message) {
		this(code, message,null);
	}
	
	public ExceptionResponse(int code, String message, List<FieldError> errors) {
		super();
		this.code = code;
		this.message = message;
		this.errors = errors;
	}
	
	public ExceptionResponse(Errors error, List<FieldError> errors){
		this(error.getCode(), error.getMessage(), errors);
	}
	
	public ExceptionResponse(Errors error){
		this(error, (List<FieldError>) null);
	}
	
	public ExceptionResponse(Errors error, BindingResult bindingResult){
		this(error.getCode(), error.getMessage(), getFieldErrors(bindingResult));
	}
	
	private static List<FieldError> getFieldErrors(BindingResult bindingResult){
		return bindingResult.getFieldErrors().stream()
		.map(e -> {
			FieldError fieldError = new FieldError();
			fieldError.setField(e.getField());
			fieldError.setMessage(e.getDefaultMessage());
			fieldError.setType(e.getCode());
			return fieldError;
		}).collect(Collectors.toList());
	}
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<FieldError> getErrors() {
		return errors;
	}
	public void setErrors(List<FieldError> errors) {
		this.errors = errors;
	}	
	
}
