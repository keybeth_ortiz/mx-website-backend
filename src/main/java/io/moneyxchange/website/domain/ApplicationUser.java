package io.moneyxchange.website.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import io.moneyxchange.website.captcha.ValidRecaptcha;
import io.moneyxchange.website.validation.Maxlength;
import io.moneyxchange.website.validation.Minlength;

@Entity
@Table(name="user")
public class ApplicationUser {
    
    public interface Existing {}
 
    public interface Login {}
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Min(value=1,groups=Existing.class)
    private long id;
    
    @NotBlank(groups = {Login.class,Existing.class})
    @Email(groups = {Login.class,Existing.class})
    @Column(unique = true)
    private String email;

    @NotEmpty(groups = {Login.class,Existing.class})
    @Minlength(value=8, groups = Login.class)
    @Maxlength(value=16, groups = Login.class)
    @JsonProperty(access = Access.WRITE_ONLY)
    private String password;
    
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
        name = "user_role", 
        joinColumns = { @JoinColumn(name = "user_id") }, 
        inverseJoinColumns = { @JoinColumn(name = "role_id") }
    )
    private Set<Role> roles = new HashSet<>();
    
    @Transient
    @NotBlank(groups = Login.class)
    @ValidRecaptcha(groups = Login.class)
    private String recaptcha;

    public long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRecaptcha() {
		return recaptcha;
	}

	public void setRecaptcha(String recaptcha) {
		this.recaptcha = recaptcha;
	}
}
