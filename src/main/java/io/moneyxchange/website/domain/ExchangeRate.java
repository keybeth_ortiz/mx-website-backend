package io.moneyxchange.website.domain;

import java.io.Serializable;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="exchange_rate", uniqueConstraints = {@UniqueConstraint(columnNames = {"base_currency_id", "to_currency_id"})})
public class ExchangeRate implements Serializable{

	private static final long serialVersionUID = -5416473099501501467L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "base_currency_id", nullable = false)
	private Currency baseCurrency;
	
	@ManyToOne
	@JoinColumn(name = "to_currency_id", nullable = false)
	private Currency toCurrency;
	
	@Column(name = "rate", nullable = false)
	private Double rate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Currency getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(Currency baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public Currency getToCurrency() {
		return toCurrency;
	}

	public void setToCurrency(Currency toCurrency) {
		this.toCurrency = toCurrency;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}
	
	@Transient
	public String retrieveToCurrencySymbol(){
		return Optional.ofNullable(toCurrency).map(Currency::getSymbol).get();
	}
}
