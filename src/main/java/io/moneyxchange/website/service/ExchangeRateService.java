package io.moneyxchange.website.service;

import java.util.Map;

public interface ExchangeRateService {

	/**
	 * Quote the actual rate between two currencies
	 * 
	 * @param baseCurrency
	 * @param toCurrency
	 * @return
	 */
	public Map<String, Double> quoteRate(String baseCurrency, String toCurrency);
	
	/**
	 * Get actual exchange rate of the currencies for the given currency 
	 * 	
	 * @param currency
	 * @return
	 */
	public Map<String, Double> getActualRatesFromCurrency(String currency);
	
}
