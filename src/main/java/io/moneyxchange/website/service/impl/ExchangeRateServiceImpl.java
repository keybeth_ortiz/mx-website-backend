package io.moneyxchange.website.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.moneyxchange.website.domain.ExchangeRate;
import io.moneyxchange.website.repositories.ExchangeRateRepository;
import io.moneyxchange.website.service.ExchangeRateService;

@Service
@Transactional
public class ExchangeRateServiceImpl implements ExchangeRateService {
	
	@Autowired
	private ExchangeRateRepository repository;

	@Override
	public Map<String, Double> quoteRate(String baseCurrency, String toCurrency ){
		ExchangeRate exchangeRate = repository.findByBaseCurrencySymbolAndToCurrencySymbol(baseCurrency, toCurrency);
		return Stream.of(exchangeRate).filter(er -> Optional.ofNullable(er).isPresent() ).collect( Collectors.toMap(ExchangeRate::retrieveToCurrencySymbol, ExchangeRate::getRate));
	}

	@Override
	public Map<String, Double> getActualRatesFromCurrency(String currency) {
		List<ExchangeRate> exchangeRates = repository.findByBaseCurrencySymbol(currency);
		return exchangeRates.stream().collect( Collectors.toMap(ExchangeRate::retrieveToCurrencySymbol, ExchangeRate::getRate) );
	}
	
}
